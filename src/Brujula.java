public class Brujula {
    int norte=0;
    int sur = 0;
    int este = 0;
    int oeste =0;
    int [] entrada = {};
    String resultado ="";



    public int getNorthSouth(int [] input){
        for (int i = 0; i<input.length; i++) {
            if (input[i] == 1) {
                norte++;
            } else if (input[i] == 2) {
                sur++;
            }
        }
        return (norte-sur);
    }
    public int getEastWest(int [] input){
        for (int i = 0; i<input.length; i++) {
            if (input[i] == 3) {
                este++;
            } else if (input[i] == 4) {
                oeste++;
            }
        }
        return (este-oeste);
    }

    public String caminar(int [] input){
        String resultat ="";

        int ns = getNorthSouth(input);
        int ew = getEastWest(input);
        if (ns > 0){
            resultat = resultat + ns+"N";
        }else if (ns < 0){
            resultat = resultat + -ns+"S";
        }

        if (ew > 0){
            resultat = resultat + ew+"E";
        }else if( ew < 0){
            resultat = resultat + -ew+"O";
        }

        return resultat;
    }
}

