import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BrujulaTest extends Brujula {

    @Test
    void T_NS1() {
        int[] input = {1, 1, 1, 2, 2, 2};
        assertEquals(0, getNorthSouth(input));
    }

    @Test
    void T_NS2() {
        int[] input = {1, 1, 1, 1, 2, 2, 2};
        assertEquals(1, getNorthSouth(input));
    }

    @Test
    void T_NS3() {
        int[] input = {1, 1, 2, 2, 2};
        assertEquals(-1, getNorthSouth(input));
    }

    @Test
    void T_EW1() {
        int[] input = {3, 3, 3, 4, 4, 4};
        assertEquals(0, getEastWest(input));
    }

    @Test
    void T_EW2() {
        int[] input = {3, 3, 3, 3, 4, 4, 4};
        assertEquals(1, getEastWest(input));
    }

    @Test
    void T_EW3() {
        int[] input = {3, 3, 4, 4, 4};
        assertEquals(-1, getEastWest(input));
    }

    @Test
    void T_S1() {
        int[] input = {1, 1, 1, 1, 1};
        assertEquals("5N", caminar(input));
    }

    @Test
    void T_S2() {
        int[] input = {2, 2, 2, 2, 2};
        assertEquals("5S", caminar(input));
    }

    @Test
    void T_S3() {
        int[] input = {3, 3, 3, 3, 3};
        assertEquals("5E", caminar(input));
    }

    @Test
    void T_S4() {
        int[] input = {4, 4, 4, 4, 4};
        assertEquals("5O", caminar(input));
    }

    @Test
    void T_S5() {
        int[] input = {1, 1, 4, 4, 4};
        assertEquals("2N3O", caminar(input));
    }

    @Test
    void T_S6() {
        int[] input = {2, 2, 3, 3, 3};
        assertEquals("2S3E", caminar(input));
    }

    @Test
    void T_S7() {
        int[] input = {1, 1, 3, 4, 4};
        assertEquals("2N1O", caminar(input));
    }

    @Test
    void T_S8() {
        int[] input = {2, 2, 1, 3, 3};
        assertEquals("1S2E", caminar(input));
    }
}

